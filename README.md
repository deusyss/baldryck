BALDRYCK
========

Welcome on the Baldryck Mercurial deposit

What is Baldrick?
=================

Baldryck is a tool to improve the quality of Python project. It's writed in Python (Python/Django) for Python project.

If all works fine, V1.0.0 will offer quality metric tools and continuous integration.

How it would work?
==================

The graphical interface will let user define a project configuration file (PCF), using GUI. This will generate json file. This file will need to be integrated to Mercurial or GIT project deposit.

Then user will add a newproject to Baldrick, and will precise the PCF to use in the deposit.

Each declared project will be checked out,and the PCF will be used to supply the different script used in Baldrick.

So several tools wil be passed,and all metrics will be return to user under graphical restitution.

The PCF will let flexibility to user. So it can modify variables like version number without to interact with Baldrick tool.

Documentation
=============

http://baldryck.readthedocs.io/en/latest/index.html

Roadmap
=======

The main steps of Baldrick development will be the following:

  * Functional documentation write
  * Architecture design
  * Technical documentationwrite
  * Graphical server design
  * Quality tools integration
  * Buildbot integration

Dependencies
============

In order to work, Baldrick requires following packages:

  * Pylint
  * McCabe
  * Radon
  * Pydocstyle
  * unittest
  * coverage
  * robotframework
  * Fabric
  * Pygal
  * BuildBot
  
Authors
=======

Authors are two professionnal Python developers:

  * GALODE Alexandre, technical manager & Python expert
  * DIVET Benjamin, full stack Python developer & graphical/ergonome expert 