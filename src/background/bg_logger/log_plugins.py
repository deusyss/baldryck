"""
Bladryck plugins log and error mechanism, using decorators
"""
import os.path as osp
import logging
from logging.handlers import RotatingFileHandler
import time


class PluginsLogger(object):

    def __init__(self):
        pass

    @staticmethod
    def set_log_cfg(output_path):
        """
        
        :return: 
        """
        base_path = osp.abspath(osp.dirname(__file__))
        log_file = osp.join(base_path, "plugins_log.txt")
        format_str = "%(asctime)s -- %(levelname)s -- %(message)s"

        PluginsLogger.formatter_info = logging.Formatter(format_str)

        PluginsLogger.logger_info = logging.getLogger("info_log")
        PluginsLogger.handler_info = logging.handlers.RotatingFileHandler(log_file, mode="a",
                                                                          maxBytes=5000,
                                                                          backupCount=3,
                                                                          encoding="utf-8")
        PluginsLogger.handler_info.setFormatter(PluginsLogger.formatter_info)
        PluginsLogger.logger_info.setLevel(logging.INFO)
        PluginsLogger.logger_info.addHandler(PluginsLogger.handler_info)

    @staticmethod
    def plugins_logger(function):
        """
        
        :return: 
        """
        def ma_fonction_decoree(*args, **kwargs):
            start = time.time()

            try:
                result = function(*args, **kwargs)
                stop = time.time()
                exec_time = stop - start
                PluginsLogger.logger_info.info(
                    "Method {} executed in {} s".format(function.__name__, exec_time))
            except Exception as err:
                PluginsLogger.logger_info.critical(
                    "Error during method {} execution: {}".format(function.__name__, err))
                PluginsLogger.logger_info.critical("No named arguments: {}".format(args))
                PluginsLogger.logger_info.critical("Named arguments: {}".format(kwargs))
                raise
            else:
                return result

        return ma_fonction_decoree
