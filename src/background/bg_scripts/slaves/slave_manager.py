"""

"""
import os
import os.path as osp
import sys
import time

import subprocess

import sched

from fabric.api import env, run, put, get

from slave import Slave


class SlavesManager(object):

    def __init__(self):
        self.base_path = osp.abspath(osp.dirname(__file__))
        self.dict_slaves = dict()
        self.dict_slaves_info = dict()

    def add_slave(self, name, slave_address, slave_port=22, host_slave=False):
        try:
            isinstance(name, str)
            isinstance(slave_address, str)
            isinstance(slave_port, int)
            isinstance(host_slave, bool)
        except AssertionError as err:
            raise err
        else:
            self.dict_slaves_info[name] = dict()

            self.dict_slaves[name] = Slave(name, slave_address, slave_port, host_slave)
            self.dict_slaves_info[name]["ADDRESS"] = slave_address
            self.dict_slaves_info[name]["PORT"] = slave_port
            self.dict_slaves_info[name]["TYPE"] = host_slave

    def del_slave(self, name):
        try:
            isinstance(name, str)
        except AssertionError as err:
            raise err
        else:
            del self.dict_slaves[name]

    def get_slaves_list(self):
        return [name for name in self.dict_slaves.keys()]


if __name__ == "__main__":
    slave_name = "test"
    slave_ip = "192.168.1.3"

    smgr = SlavesManager()
    smgr.add_slave(slave_name, slave_ip)
    smgr.dict_slaves[slave_name].launch_job("HG", "https://deusyss@bitbucket.org/deusyss/baldryck")
    # print(smgr.dict_pid)
    # print(smgr.dict_pid)
