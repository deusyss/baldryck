"""

"""
import os
import os.path as osp
import sys
import time

import subprocess

import sched

from fabric.api import env, run, put, get

from vcs import GitVcs, MercurialVcs, SvnVcs


class SlaveProcess(object):

    REPO_HG = "HG"
    REPO_GIT = "GIT"
    REPO_SVN = "SVN"

    def __init__(self, queue_order, queue_feedback, address, port=22, timer_s=0.05,
                 host_slave=False):
        self.base_path = osp.abspath(osp.dirname(__file__))
        self.baldryck_slave_set_script_path = osp.join(self.base_path, "baldryck_slave_set.py")
        self.baldryck_slave_set_path = osp.join(self.base_path, "..", "baldryck_slave_set")

        self.queue_order = queue_order
        self.queue_feedback = queue_feedback
        self.host_slave = host_slave

        self.address = address
        self.port = port
        self.timer_s = timer_s

        slave_home_cmd = "python -c \"import os; print(os.path.expanduser('~'))\""

        if self.host_slave:
            ps = subprocess.Popen(slave_home_cmd)
            ps.wait()
            # todo, get output std
        else:
            env.host_string = "{}:{}".format(self.address, self.port)
            self.slave_home_path = self.__exec_cmd_on_slave(slave_home_cmd)

        self.__create_path()
        self.scheduler = sched.scheduler(time.time, time.sleep)

    def __del__(self):
        self.scheduler.cancel(self.queue_event)
        sys.exit()

    def __create_path(self):
        self.slave_folder_path = osp.join(self.slave_home_path, "Baldryck_slave")
        self.slave_set_folder_path = osp.join(self.slave_folder_path, "baldryck_slave_set")
        self.venv_path = osp.join(self.slave_set_folder_path, "baldryck_venv")
        self.dependencies_folder_path = osp.join(self.slave_set_folder_path, "dependencies")
        self.activate_path = osp.join(self.venv_path, "bin", "activate")
        self.requirement_path = osp.join(self.dependencies_folder_path, "requirements.txt")

        for filename in os.listdir(osp.join(self.baldryck_slave_set_path, "dependencies")):
            if filename.lower().startswith("pip"):
                self.pip_whl_path = osp.join(self.dependencies_folder_path, filename)

    def launch_scheduler(self):
        self.queue_event = self.scheduler.enter(self.timer_s, 1, self.check_order_queue)

        try:
            self.scheduler.run()
        except KeyboardInterrupt as err:
            print(err)
        except Exception as err:
            print(err)

    def check_order_queue(self):
        try:
            if not self.queue_order.empty():
                tmp_data = self.queue_order.get()
                method_name = "{}".format(tmp_data[0])
                # method_name = "_RemoteSlave__{}".format(tmp_data[0])
                if len(tmp_data) > 1:
                    args = tmp_data[1]
                    kwargs = tmp_data[2]
                method_to_call = getattr(self, method_name)
                if method_to_call:
                    if len(tmp_data) == 1:
                        method_to_call()
                    else:
                        method_to_call(*args, **kwargs)
        except Exception as err:
            print(err)

        self.queue_event = self.scheduler.enter(self.timer_s, 1, self.check_order_queue)

    def create_baldryck_slave_set(self):
        ps = subprocess.Popen("python3 \"{}\"".format(self.baldryck_slave_set_script_path),
                              shell=True)
        ps.wait()

    def create_dst_folder_on_slave(self):
        cmd = "mkdir -p {0}".format(self.slave_folder_path)

        if self.host_slave:
            pass
        else:
            self.delete_baldryck_slave_folder()
            self.__exec_cmd_on_slave(cmd)

    def push_set_on_home_slave(self):
        if self.host_slave:
            pass
        else:
            self.__push_folder_to_slave(self.baldryck_slave_set_path, self.slave_folder_path)

    def venv_deploiement(self):
        cmd_activate_venv = ". {}".format(self.activate_path)
        cmd_venv_creation = "virtualenv --no-pip -p python3 {}".format(self.venv_path)
        cmd_install_pip = "{0} && python {1}/pip install --no-index {1}".format(cmd_activate_venv,
                                                                                self.pip_whl_path)
        cmd_install_dependencies = "{} && ".format(cmd_activate_venv)
        cmd_install_dependencies += "pip install -f {} -r {}".format(self.dependencies_folder_path,
                                                                     self.requirement_path)

        if self.host_slave:
            pass
        else:
            self.__exec_cmd_on_slave(cmd_venv_creation)
            self.__exec_cmd_on_slave(cmd_install_pip)
            self.__exec_cmd_on_slave(cmd_install_dependencies)
            # slave_script_path = osp.join(self.slave_folder_path, "baldryck_slave_set",
            #                              "create_venv.py")
            # cmd = "python3 \"{}\"".format(slave_script_path)
            # self.__exec_cmd(cmd)

    def delete_baldryck_slave_folder(self):
        cmd = "rm -rf {0}".format(self.slave_folder_path)
        if self.host_slave:
            pass
        else:
            self.__exec_cmd_on_slave(cmd)

    def clone_repo(self, repo_type, repo_address, repo_branch=None, repo_tag=None):
        print("type:", repo_type)
        repo_path = osp.join(self.slave_folder_path, "repository")

        if repo_type == "HG":
            repo_cmd = MercurialVcs(repo_path, repo_address)
        elif repo_type == "GIT":
            repo_cmd = GitVcs(repo_path, repo_address)
        else:
            repo_cmd = SvnVcs(repo_path, repo_address)

        if repo_type in ["HG", "GIT", "SVN"]:
            self.__exec_cmd_on_slave(repo_cmd.cmd_clone)

            if repo_branch:
                self.__exec_cmd_on_slave(repo_cmd.cmd_change_branch.format(repo_branch))

            if repo_tag:
                self.__exec_cmd_on_slave(repo_cmd.cmd_change_tag.format(repo_tag))

    def __push_file_to_slave(self, src_file_path, dst_file_dir):
        """
        Allows to push a single file on remote slave

        :param src_file_path: the absolute src file, on host
        :type src_file_path: str
        :param dst_file_dir: the absolute dst folder, on remote slave
        :type dst_file_dir: str
        """
        try:
            isinstance(src_file_path, str)
            isinstance(dst_file_dir, str)
        except AssertionError as err:
            raise err
        else:
            output = self.__exec_cmd_on_slave("mkdir -p {}/".format(dst_file_dir))
            output += str(put(src_file_path, remote_path=dst_file_dir))
            return output

    def __pull_file_from_slave(self, src_file_path, dst_file_dir):
        """
        Allows to pull a single file from remote slave

        :param src_file_path: the absolute src file, on remote slave
        :type src_file_path: str
        :param dst_file_dir: the absolute dst folder, on host
        :type dst_file_dir: str
        """
        try:
            isinstance(src_file_path, str)
            isinstance(dst_file_dir, str)
        except AssertionError as err:
            raise err
        else:
            output = str(get(src_file_path, local_path=dst_file_dir))
            return output

    def __push_folder_to_slave(self, src_folder_path, dst_folder_path):
        """
        Allows to push recursively a folder on remote slave

        :param src_folder_path: the absolute src folder, on host
        :type src_folder_path: str
        :param dst_folder_path: the absolute dst folder, on remote slave
        :type dst_folder_path: str
        """
        try:
            isinstance(src_folder_path, str)
            isinstance(dst_folder_path, str)
        except AssertionError as err:
            raise err
        else:
            output = self.__push_file_to_slave(src_folder_path, dst_folder_path)
            return output

    def __pull_folder_from_slave(self, src_folder_path, dst_folder_path):
        """
        Allows to pull recursively a folder from remote slave

        :param src_folder_path: the absolute src folder, on remote slave
        :type src_folder_path: str
        :param dst_folder_path: the absolute dst folder, on host
        :type dst_folder_path: str
        """
        try:
            isinstance(src_folder_path, str)
            isinstance(dst_folder_path, str)
        except AssertionError as err:
            raise err
        else:
            output = self.__pull_file_from_slave(src_folder_path, dst_folder_path)
            return output

    def __exec_cmd_on_slave(self, cmd):
        """
        Allows to execute a remote command on slave

        :param cmd: the command to execute
        :type cmd: str 
        """
        try:
            isinstance(cmd, str)
        except AssertionError as err:
            raise err
        else:
            output = str(run(cmd))
            return output


class HostSlaveProcess(SlaveProcess):
    def __init__(self, queue_order, queue_feedback, address, port=22, timer_s=0.05):
        try:
            isinstance(address, str)
            isinstance(port, int)
            isinstance(timer_s, float)
        except AssertionError as err:
            raise err
        else:
            super().__init__(queue_order, queue_feedback, address, port, timer_s, True)
            self.launch_scheduler()


class RemoteSlaveProcess(SlaveProcess):
    """
    Allows to manage all specifics actions for each slave
    """

    def __init__(self, queue_order, queue_feedback, address, port=22, timer_s=0.05):
        """
        :param address: the ip address of the remote slave machine
        :type address: str 
        :param port: the port to establish connection. 22 per default
        :type port: int
        """
        try:
            isinstance(address, str)
            isinstance(port, int)
            isinstance(timer_s, float)
        except AssertionError as err:
            raise err
        else:
            super().__init__(queue_order, queue_feedback, address, port, timer_s, False)
            self.launch_scheduler()
