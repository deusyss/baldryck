"""

"""
import os
import os.path as osp
import signal
import multiprocessing
import time
import sched

from slave_ps import RemoteSlaveProcess, HostSlaveProcess


class Slave(object):

    def __init__(self, name, slave_address, slave_port=22, host_slave=False):
        if host_slave:
            slave_class = HostSlaveProcess
        else:
            slave_class = RemoteSlaveProcess

        self.queue_order = multiprocessing.Queue()
        self.queue_feedback = multiprocessing.Queue()
        self.slave_process = multiprocessing.Process(target=slave_class,
                                                     args=(self.queue_order,
                                                           self.queue_feedback,
                                                           slave_address,
                                                           slave_port)
                                                     )

        self.host_pid = None

        self.slave_process.start()

    def __del__(self):
        # pid = int(self.dict_pid[name])
        self.queue_order.close()
        self.queue_feedback.close()
        self.slave_process.terminate()
        try:
            os.kill(self.host_pid, signal.SIGKILL)
        except Exception as err:
            print(err)

    def stop(self):
        pass

    def start(self):
        pass

    def __push_order(self):
        pass

    def __pull_feedback(self):
        pass

    def is_online(self):
        pass

    def is_working(self):
        pass

    def launch_job(self, repo_type, repo_address, repo_branch=None, repo_tag=None):
        # todo: missing feedback output
        # Create Baldryck slave set
        self.push_order("create_baldryck_slave_set")
        self.push_order("create_dst_folder_on_slave")
        self.push_order("push_set_on_home_slave")
        self.push_order("venv_deploiement")
        self.push_order("clone_repo", [repo_type, repo_address], {"repo_branch": repo_branch,
                                                                  "repo_tag": repo_tag})

    def clean_job(self):
        self.queue_order.put(["create_baldryck_slave_set"])

    def read_feedback_queues(self):
        pass

    def push_order(self, order, args=[], kwargs={}):
        self.queue_order.put([order, args, kwargs])

    def pull_feedback(self, name):
        pass

    def get_host_pid(self):
        pass

    def get_slave_pid(self):
        pass
