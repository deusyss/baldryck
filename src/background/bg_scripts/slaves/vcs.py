"""
Allows to manage all vcs type
"""


class GitVcs(object):

    def __init__(self, dst_folder, repo_address):
        self.dst_folder = dst_folder
        self.cmd_clone = "git clone {} {}".format(repo_address, self.dst_folder)
        self.cmd_change_branch = "git checkout {}"
        self.cmd_change_tag = "git checkout tags/{}"
        self.cmd_last_master = "git checkout master"


class MercurialVcs(object):

    def __init__(self, dst_folder, repo_address):
        self.dst_folder = dst_folder
        self.cmd_clone = "hg clone {} {}".format(repo_address, self.dst_folder)
        self.cmd_change_branch = "hg up {}"
        self.cmd_change_tag = "hg up {}"
        self.cmd_last_master = "git checkout default"


class SvnVcs(object):

    def __init__(self, dst_folder, repo_address):
        self.dst_folder = dst_folder
        self.cmd_clone = "svn checkout {} {}".format(repo_address, self.dst_folder)
        self.cmd_change_branch = "svn switch {}/branches/".format(repo_address) + "{}"
        self.cmd_change_tag = "svn switch {}"
        self.cmd_last_master = "svn checkout default"
