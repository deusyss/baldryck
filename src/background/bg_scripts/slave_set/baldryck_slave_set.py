"""
Baldryck remote necessary preparation. Prepare scripts and wheels for distant slave
"""
import os
import os.path as osp
import sys
import subprocess
import shutil

###################################################################################################
# We determine basic path
# host_home_path = osp.abspath(os.path.expanduser('~'))
# base_path = osp.abspath(osp.dirname(__file__))
#
# tmp_host_slave_set_folder_path = osp.join(host_home_path, "baldryck_slave_set")
# tmp_host_wheel_folder_path = osp.join(host_home_path, "baldryck_ps_wheel")
#
# remote_dir_path = osp.join(base_path, "..", "baldryck_slave_set")
# dependencies_dir_path = osp.join(remote_dir_path, "dependencies")
# output_dir_path = osp.join(remote_dir_path, "outputs")
# log_output_path = osp.join(base_path, "..", "baldryck_bg_logs")
# setup_path = osp.join(base_path, "..", "baldryck_wheel", "setup.py")
# baldryck_wheel_path = osp.join(base_path, "..", "baldryck_wheel")
# baldryck_dependencies_path = osp.join(base_path, "..", "..", "baldryck_dependencies")
#
# sys.path.append(baldryck_wheel_path)
#
#
#
# plugins_mgr = PluginsManager(log_output_path)
# list_plugins = plugins_mgr.list_plugins_name
#
# cmd_clear_dep = "rm -rf '{}/*'".format(dependencies_dir_path)
# cmd_clear_output = "rm -rf '{}/*'".format(output_dir_path)
# cmd_create_baldryck_pkg = "python3 \"{}\" bdist_wheel".format(setup_path)


class BaldryckSlave(object):

    def __init__(self, slave_name):
        self.__create_paths(slave_name)
        self.__manage_plugins()

    def __create_paths(self, slave_name):
        self.host_home_path = osp.abspath(os.path.expanduser('~'))
        self.base_path = osp.abspath(osp.dirname(__file__))

        self.tmp_slave_set_folder_path = osp.join(self.host_home_path,
                                                       "baldryck_slave_set_{}".format(slave_name))
        self.tmp_wheel_folder_path = osp.join(self.host_home_path,
                                                   "baldryck_ps_wheel_{}".format(slave_name))

        self.dependencies_dir_path = osp.join(self.tmp_slave_set_folder_path, "dependencies")
        self.output_dir_path = osp.join(self.tmp_slave_set_folder_path, "outputs")

        self.baldryck_wheel_env_path = osp.join(self.base_path, "..", "baldryck_wheel")

        self.remote_dir_path = osp.join(self.base_path, "..", "baldryck_slave_set")
        self.plugins_path = osp.abspath(osp.join(self.base_path, "..", "..", ".."))
        self.setup_path = osp.join(self.tmp_wheel_folder_path, "setup.py")

        self.log_output_path = osp.join(self.base_path, "..", "baldryck_bg_logs")
        self.baldryck_wheel_path = osp.join(self.base_path, "..", "baldryck_wheel")
        self.baldryck_dependencies_path = osp.abspath(osp.join(self.base_path, "..", "..",
                                                               "bg_dependencies"))

    def __manage_plugins(self):
        sys.path.append(self.plugins_path)
        from plugins import PluginsManager

        self.plugins_mgr = PluginsManager()
        self.list_plugins = self.plugins_mgr.list_plugins_name

    def __create_folder(self, folder_path):
        try:
            shutil.rmtree(folder_path)
        except Exception:
            pass

        try:
            os.makedirs(folder_path)
        except Exception:
            pass

    def create_slave_set(self):
        self.__create_folder(self.tmp_slave_set_folder_path)
        self.__create_folder(self.dependencies_dir_path)
        self.__create_folder(self.output_dir_path)
        self.__copy_baldryck_dependencies()
        self.__copy_plugins_dependencies()
        self.__copy_wheel_to_slave_set()
        self.__create_req_file()

    def __copy_baldryck_dependencies(self):
        for filename in os.listdir(self.baldryck_dependencies_path):
            shutil.copy(osp.join(self.baldryck_dependencies_path, filename),
                        self.dependencies_dir_path)

    def __copy_plugins_dependencies(self):
        for plugin_name in self.list_plugins:
            plugin_dep_path = osp.join(self.plugins_path, "plugins",
                                       plugin_name.lower(), "dependencies")

            try:
                os.makedirs(osp.join(self.output_dir_path, plugin_name))
            except Exception as err:
                pass

            for filename in os.listdir(plugin_dep_path):
                shutil.copy(osp.join(plugin_dep_path, filename), self.dependencies_dir_path)

    def __create_req_file(self):
        requirement_list = list()
        for filename in os.listdir(self.dependencies_dir_path):
            if not filename.lower().startswith("pip"):
                requirement_list.append(filename.split("-")[0])

        requirement_list = list(set(requirement_list))

        req_file_path = osp.join(self.dependencies_dir_path, "requirements.txt")
        with open(req_file_path, "w") as req_file:
            for pkg in requirement_list:
                req_file.write("{}\n".format(pkg))

    def create_baldryck_wheel(self):
        self.__create_folder(self.tmp_wheel_folder_path)
        self.__set_wheel_env()
        self.__generate_wheel()

    def cp_baldryck_wheel_in_slave_set(self):
        for filename in os.listdir(osp.join(self.tmp_wheel_folder_path, "dist")):
            shutil.copy(osp.join(self.tmp_wheel_folder_path, "dist", filename),
                        self.dependencies_dir_path)

    def __set_wheel_env(self):
        baldryck_pas_path = osp.join(self.tmp_wheel_folder_path, "baldryck_ps")
        self.__create_folder(baldryck_pas_path)

        shutil.copy(osp.join(self.baldryck_wheel_env_path, "setup.py"), self.tmp_wheel_folder_path)
        shutil.copy(osp.join(self.baldryck_wheel_env_path, "setup.cfg"), self.tmp_wheel_folder_path)

        shutil.copy(osp.join(self.baldryck_wheel_env_path, "README.txt"),
                    self.tmp_wheel_folder_path)

        shutil.copy(osp.join(self.baldryck_wheel_env_path, "__init__.py"), baldryck_pas_path)
        shutil.copy(osp.join(self.baldryck_wheel_env_path, "analyze_deposit.py"), baldryck_pas_path)

        for folder, folders, files in os.walk(osp.join(self.plugins_path, "plugins")):
            rel_path = osp.relpath(folder, osp.join(self.plugins_path, "plugins"))
            tmp_path_dst = osp.normpath(osp.join(self.tmp_wheel_folder_path, "baldryck_ps",
                                                 "plugins", rel_path))

            if not osp.exists(tmp_path_dst):
                self.__create_folder(tmp_path_dst)

            for filename in files:
                tmp_path_src = osp.join(folder, filename)
                shutil.copy(tmp_path_src, tmp_path_dst)

    def __generate_wheel(self):
        cmd_create_baldryck_pkg = "python3 \"{}\" bdist_wheel".format(self.setup_path)

        os.chdir(self.tmp_wheel_folder_path)
        ps = subprocess.Popen(cmd_create_baldryck_pkg, shell=True)
        ps.wait()

    def __copy_wheel_to_slave_set(self):
        for filename in os.listdir(osp.join(self.tmp_wheel_folder_path, "dist")):
            shutil.copy(osp.join(self.tmp_wheel_folder_path, "dist", filename),
                        self.dependencies_dir_path)

    def clean_tmp_folders(self):
        try:
            shutil.rmtree(self.tmp_slave_set_folder_path)
        except Exception:
            pass

        try:
            shutil.rmtree(self.tmp_wheel_folder_path)
        except Exception:
            pass

if __name__ == "__main__":
    bs = BaldryckSlave("toto")
    bs.create_baldryck_wheel()
    bs.create_slave_set()
    bs.cp_baldryck_wheel_in_slave_set()
    bs.clean_tmp_folders()
