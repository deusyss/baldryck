import os
import os.path as osp
import subprocess


base_path = osp.abspath(osp.dirname(__file__))
venv_path = osp.join(base_path, "baldryck_venv")
dependencies_folder_path = osp.join(base_path, "dependencies")
activate_path = osp.join(venv_path, "bin", "activate")
requirement_path = osp.join(dependencies_folder_path, "requirements.txt")


for filename in os.listdir(dependencies_folder_path):
    if filename.lower().startswith("pip"):
        pip_whl_path = osp.join(dependencies_folder_path, filename)


cmd_activate_venv = ". {}".format(activate_path)


# We create venv
cmd_venv_creation = "virtualenv --no-pip -p python3 {}".format(venv_path)
ps = subprocess.Popen(cmd_venv_creation, shell=True)
ps.wait()


# We install pip in venv
cmd_install_pip = "{0} && python {1}/pip install --no-index {1}".format(cmd_activate_venv,
                                                                        pip_whl_path)
ps = subprocess.Popen(cmd_install_pip, shell=True)
ps.wait()


# We install all dependencies
cmd_install_dependencies = "{} && pip install -f {} -r {}".format(cmd_activate_venv,
                                                                  dependencies_folder_path,
                                                                  requirement_path)
ps = subprocess.Popen(cmd_install_dependencies, shell=True)
ps.wait()



