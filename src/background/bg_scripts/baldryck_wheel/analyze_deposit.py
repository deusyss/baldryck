"""
Baldryck script for deposit analyze
"""
import os
import os.path as osp
import json

import baldryck_ps.plugins


class DepositAnalyzer(object):

    def __init__(self, abs_deposit_path, exclude_dirs, exclude_files, exclude_ext,
                 rel_analyze_path="./baldryck_analyze"):
        """
        Allows to analyze the deposit to get a list of deposit structure. It will contains \
         only useful folders and files
        :param exclude_dirs: relative path, to src, of directories to exclude 
        :type exclude_dirs: list
        :param exclude_files: relative path, to src, of files to exclude, with extension
        :type exclude_files: list
        :param exclude_ext: extension to exclude
        :type exclude_ext: list
        """
        self.src_path = abs_deposit_path
        self.dst_path = osp.join(abs_deposit_path, rel_analyze_path)

        self.list_exclude_dirs = list()
        self.list_exclude_files = list()
        self.dict_analyze = dict()

        self.list_exclude_extensions = exclude_ext

        self.dict_analyze["SRC"] = self.src_path
        self.dict_analyze["DST"] = self.dst_path
        self.dict_analyze["EXCLUDE_DIRS"] = self.list_exclude_dirs
        self.dict_analyze["EXCLUDE_FILES"] = self.list_exclude_files
        self.dict_analyze["EXCLUDE_EXT"] = self.list_exclude_extensions

        for folder in exclude_dirs:
            self.list_exclude_dirs.append(osp.join(self.src_path, folder))

        for filename in exclude_files:
            self.list_exclude_files.append(osp.join(self.src_path, filename))

    def get_files_path_by_ext(self, extension):
        return self.dict_analyze.get(extension.upper(), None)

    def get_complete_analyze_dict(self):
        return self.dict_analyze

    def scan_deposit(self):
        """
        Allows to create the dictionnary 
        :return: 
        """
        self.dict_analyze["FOLDERS"] = list()

        for folder, subfolders, files in os.walk(self.src_path):
            if folder not in self.list_exclude_dirs:
                self.dict_analyze["FOLDERS"].append(folder)
                for filename in files:
                    file_path = osp.join(folder, filename)
                    extension = filename.split(".")[-1]

                    if not self.dict_analyze.get(extension.upper(), None):
                        self.dict_analyze[extension.upper()] = list()

                    if file_path not in self.list_exclude_files and \
                       extension not in self.list_exclude_extensions:
                        self.dict_analyze[extension.upper()].append(file_path)


class PluginsLauncher(object):

    def __init__(self):
        self.plugins_mgr = plugins.PluginsManager()
        self.dict_result = dict()

    def launch_a_plugin(self, plugin_name, dict_analyse):
        pass

    def launch_all(self, dict_analyse):
        list_plugins = [plugin for plugin in self.plugins_mgr.__dict__
                       if plugin.startswith("Plugin")]

        for plugin_name in list_plugins:
            class_ = getattr(self.plugins_mgr, plugin_name)
            plugin = class_(dict_analyse)
            plugin.launch()
            self.dict_result[plugin_name.upper()] = plugin.get_result()

