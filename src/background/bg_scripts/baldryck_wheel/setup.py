import os
from setuptools import setup, find_packages

import baldryck_ps

list_pkg_to_include = [pkg for pkg in find_packages() if pkg.startswith("baldryck_ps")]

setup(name="baldryck_ps",
      version=baldryck_ps.__VERSION__,
      platforms='LINUX',
      license="GPL V3",
      description="Baldryck is a cunning plan for Python Project Quality",
      long_description=open(os.path.join(os.path.dirname(__file__), 'README.txt')).read(),
      author="DIVET Benjamin, GALODE Alexandre",
      maintainer="DIVET Benjamin, GALODE Alexandre",
      keywords="python IC quality",
      classifiers=["License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
                   "Operating System :: POSIX :: Linux",
                   "Programming Language :: Python :: 3",
                   "Topic :: Software Development :: Quality Assurance"],
      packages=list_pkg_to_include,
     )

