"""
Baldrick plugins script
"""
import importlib
import inspect
import os
import os.path as osp


class PluginsManager(object):

    def __init__(self):
        """
        Plugins manager initialisation
        """
        self.plugins_dir_path = osp.abspath(osp.dirname(__file__))
        self.list_dependencies_path = list()
        self.list_plugins_name = list()

        self.get_plugins_list()
        # self.load_plugins()

    def get_plugins_list(self):
        """
        Allows to scan the content of the plugins folder, to find plugins to import 
        
        :return: 
        """
        # We create sub-folders list
        list_folder = list()
        for folder in os.listdir(self.plugins_dir_path):
            if osp.isdir(osp.join(self.plugins_dir_path, folder)) and not folder.startswith("_"):
                list_folder.append(folder)

        # For each folder, we check if subfolder structure is compatible with a plugin
        for folder in list_folder:
            init_path = osp.join(self.plugins_dir_path, folder,  "__init__.py")
            cfg_json_path = osp.join(self.plugins_dir_path, folder, "cfg_ui.json")
            dep_path = osp.join(self.plugins_dir_path, folder, "dependencies")
            parse_path = osp.join(self.plugins_dir_path, folder, "parse.py")
            tool_path = osp.join(self.plugins_dir_path, folder, "tool.py")

            list_files_dep = [file.split(".")[-1].lower() for file in os.listdir(dep_path)]

            if osp.exists(init_path) and osp.exists(cfg_json_path) and \
                osp.exists(dep_path) and \
                osp.exists(parse_path) and osp.exists(tool_path):
                # todo: add jsonschema for cfg_ui.json file
                self.list_plugins_name.append(folder)

    def load_plugins(self):
        """
        Allows to load/reload all plugins class
        
        :return: 
        """
        list_plugin = [plugin for plugin in self.__dict__ if plugin.startswith("Plugin")]

        for plugin_name in self.list_plugins_name:
            if "Plugin{}".format(plugin_name) not in list_plugin:
                self.load_plugin(plugin_name)

    def load_plugin(self, plugin_name):
        """
        Allows to add plugin class as PluginsManager attribute, to create object 
        
        :param plugin_name: name of the package, in plugins package 
        """
        tool_path = "plugins.{}.tool".format(plugin_name)
        module = importlib.import_module(tool_path)
        for class_name, obj in inspect.getmembers(module):
            if not class_name.startswith("_"):
                # print("class:", class_name, obj)
                setattr(self, "Plugin{}".format(class_name), obj)


if __name__ == "__main__":
    plugin_mgr = PluginsManager()
