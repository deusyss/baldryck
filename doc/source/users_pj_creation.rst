****************
Project creation
****************

To create a project, you need following informations, and enter it to Baldryck:

  * Name
  * Logo
  * Deposit address/link
  * Deposit type (git or mercurial only)
  * Relative path, from root of the repository, where find `baldryck_cfg.json`
  * Users and groups authorized to see project
  * Users and groups authorized to administrate project
  * Slave to use. It can be Baldryck host for small developers structure (so choose "Host")
  * Plugins availables and which activate for this project
  * For each selected plugin (checkbox and collpase frame), the necessaries informations
