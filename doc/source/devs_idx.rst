************************
Developers documentation
************************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   devs_functional
   devs_bdd
   devs_django_structure
   devs_write_plugin
   devs_notes_for_baldryck_dev
