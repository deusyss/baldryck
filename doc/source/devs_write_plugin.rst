*********************
How to write a plugin
*********************

Write a plugin for Baldryck is very simple. Each plugin is, in reality, a simple package with a
specific structure.

First, create an empty package, and named it. It must contains:

  * An `__init__.py` file: it will contains plugin informations like version number
  * A `cfg_ui.json` file: it will define the interface needed in Baldryck for plugin configuration
  * A `tool.py` file: it will contain code to drive the tool
  * A `parse.py` file: it will contain code to convert output to Baldryck result format
  * A `dependencies` directory: it will contains all dependencies, in wheel format, for the plugin

If a this page end reading, it's still not clear for you, please analyse one of the plugins
delivered with Baldryck.

The __init__.py file
====================


The cfg_ui.json file
====================

This JSON file will describe to Baldryck the UI necessary to configure the plugin.

Here are the keys and subkeys authorized (case sensitive).


.. list-table::
  :widths: 50 50
  :header-rows: 1

  * - Key
    - Subkeys
  * - TXT: str
    -   * TITLE: str
        * DEFAULT: str
  * - LABEL: str
    -   * TITLE: str
  * - PATH: str
    -   * TITLE: str
        * DEFAULT: str
  * - CBOX: str
    -   * TITLE: str
        * DEFAULT: str
        * VALUES: list
  * - CHKBOX: str
    -   * TITLE: str
        * DEFAULT: list(str)
        * VALUES: list
  * - RADIO: str
    -   * TITLE: str
        * DEFAULT: str
        * VALUES: list
  * - SLIDE: str
    -   * TITLE: str
        * DEFAULT: str
        * MIN: float
        * MAX: float
  * - SPINBOX: str
    -   * TITLE: str
        * DEFAULT: str
        * MIN: float
        * MAX: float
        * STEP: float


The tool.py file
================


The parse.py file
=================



The dependencies folder
=======================

This folder will provide all the wheel files necessary for the good work of the plugin. This mean
that the specific tool for which plugin is created must be available too in this folder.

For example, for pylint plugin, the pylint wheel is available in its `dependencies` folder.

To be sure you have all dependencies, we recommand to you to make a `pip3 download <pkg_name>` in
the dependencies folder of your plugin. If one of wheel package is not all platform universal
(cp is in the name), so add all wheel of this package in dependencies. Pip will choose the correct
one during requirement installation.


Check if plugin is well done
============================

To check if your plugin is well done, there is two steps:

  #. Try to create the wheel from `background` folder in `Baldryck`.
  #. If success, try to deploy it in Baldryck and launch a job which will use your plugin

.. todo::
   add procedure to create wheel file, and dependencies. Add example based on pydoctest
