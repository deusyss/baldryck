******************
Slave requirements
******************

To be functional, slaves needs following requirements:

  * Python >=3.5
  * SSH
  * SCP
  * SSH authentification activated with deposit server, adn all other server needed for jobs
