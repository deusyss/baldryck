********
Licences
********

Code
====

All Baldryck code is under licence GPL V3

.. image:: _static/logo_gplv3.png
   :align: center

Feel free to participate and create new plugins to make this tool better than ever previously.

Name and logos
==============

Baldryck name and logos are under licence creative commons BY-NC-ND

.. image:: _static/logo_cc_by_nc_nd.png
   :align: center
   :scale: 10%

You can use name and logos, without any modification, in your communication.
If you'd like to use Baldryck name/logos not in native version, please contact us before.

More information on this licence on following links:
  * https://creativecommons.org/licenses/by-nc-nd/4.0/
