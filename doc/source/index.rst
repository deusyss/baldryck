.. Baldryck documentation master file, created by
   sphinx-quickstart on Fri Mar  2 21:22:21 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Baldryck's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code_idx
   devs_idx
   install_idx
   users_idx
   roadmap
   licence
