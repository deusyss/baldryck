*******
Roadmap
*******

Here is the roadmap of the Baldryck development:

Alpha version
=============

Summary
*******

Get basic Django server, Kit UI, logo, DB model, basic background features, base of the architecture, basic documentation

Alpha 1
*******

ADD: logo, Kit UI, base of the architecture, DB Model

Alpha 2
*******

ADD: Basic documentation, basic background features

Alpha 3
*******

ADD: basic Django server

.. topic:: Output

   Deb file nb 1 as `baldryck_alpha.deb`

Beta version
============

Summary
*******

Get UX/UI functionnal, unit feature fully functional

Beta 1
******

ADD: plugins process, VCS management, groups/user management, slaves management, project management

.. topic:: Output

   Deb file nb 2 as `baldryck_beta_01.deb`

Beta 2
******

ADD: plugin management, job management, pipeline management

.. topic:: Output

   Deb file nb 3 as `baldryck_beta_02.deb`

Beta 3
******

ADD: UX/UI, pipeline results restitution, historical management

.. topic:: Output

   Deb file nb 4 as `baldryck_beta_03.deb`

Release Candidate
=================

Summary
*******

Get Baldryck basically fully functional with Gunicorn, all features integrated, all basics plugins availables, Documentation for installation/users/admin

RC 1
****

ADD: basical plugins packaging, gunicorn use

.. topic:: Output

   Deb file nb 5 as `baldryck_rc_01.deb`

RC 2
****

ADD: Bugfix

.. topic:: Output

   Deb file nb 6 as `baldryck_rc_02.deb`

RC 3
****

ADD: Documentation for installation (Gunicorn)/admin/users

.. topic:: Output

   Deb file nb 7 as `baldryck_rc_03.deb`
