*******************
Master requirements
*******************

Requirements
============

To be functional, master needs following requirements:

  * Linux OS
  * Python >=3.5
  * SSH
  * SCP
  * SSH authentification activated with deposit server, adn all other server needed for jobs

Dependencies
============

To work, Baldryck use following packages. No internet connection is required as Baldryck embed all
necessaries wheel.

  * Fabric3
  * setuptools
  * wheel

