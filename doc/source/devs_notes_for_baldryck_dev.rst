*****
Notes
*****

  * Add link to check if a new versio is available
  * Communication between Baldryck and slaves with SSH/SCP, using FABRIC3. Baldryck and slave need
    having a SSH authentication to useful server.
  * When we launch a process, we get PID. SO if a user give order to stop operation, we can do a
    `kill -9` on PID, after cleaning venv and remote deposit.
  * For each tool, we display "Execution of tool xxx in progress".
  * All tools will be launched with verbose mode
  * If a "baldryck_cfg.json" is available on root of deposit, all included keys will surcharge
    default ones defined in Baldryck
  * A page will let user create a basical "baldryc_cfg.json" file.
  * Quality state will use icon as a green check, a red cross, an orange interrogation point, ...
  * Project list as a military folder, with tab as in Goldeneye64, at 90°. With project name, and
    stencil font to indicate status: green "HIGH", orange "MEDIUM", red "LOW".


Analyze process
===============

Analyze will use in entry a json, which be checked by json schema. All paths will be case
sensitive. TXT will not.

  * SRC: path type, root
  * DST: path type
  * EXCLUDE_DIRS: path type, relative to SRC
  * EXCLUDE_FILES: path type, src relative
  * EXCLUDE_EXT: str list, without "."

Will be displayed as "Project deposit analyze, step x/3"

  #. Use os walk to list all available path. Don't take folder defined as excluded
  #. Create a list of all identified folder. Could be use for obfuscation
  #. Create a dictionnary for all files. Key, in upper case, will be extension. Do not take exluded
     files and extension excluded. Each key will contain a path list, relative to src


LOGO links for inspiration
==========================

  * http://www.thebearpit.org.uk/wp-content/uploads/2017/11/blackadder-holding-image.png
  * https://ih0.redbubble.net/image.308448862.4936/papergc,441x415,w,ffffff.2u8.jpg
  * https://pbs.twimg.com/profile_images/854301723074101248/oQSDzPpy_400x400.jpg
  * https://ih0.redbubble.net/image.14537794.6831/ra%2Cunisex_tshirt%2Cx1850%2Cfafafa%3Aca443f4786%2Cfront-c%2C435%2C380%2C420%2C460-bg%2Cf8f8f8.lite-1u2.jpg
  * https://pbs.twimg.com/media/DKb1RX8WkAAHNFO.jpg
  * https://www.google.fr/imgres?imgurl=https%3A%2F%2Fcdn.rsjoomla.com%2Fimages%2Fproducts%2Fscreenshots%2F183.jpg&imgrefurl=https%3A%2F%2Fwww.rsjoomla.com%2Fjoomla-extensions%2Ffree-joomla-page-builder.html&docid=JBcXckYa66Kf3M&tbnid=T7BqsdpL6OQWrM%3A&vet=10ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwhCKAQwBA..i&w=1280&h=720&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=0ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwhCKAQwBA&iact=mrc&uact=8
  * https://wid.gy/
  * https://www.google.fr/imgres?imgurl=https%3A%2F%2Fwid.gy%2Fstatic%2Fimage%2Ffeature-drag-drop.jpg&imgrefurl=https%3A%2F%2Fwid.gy%2F&docid=V03xKB7ZRsX_eM&tbnid=qHSf54cMwi9zsM%3A&vet=10ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwg-KAAwAA..i&w=410&h=210&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=0ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwg-KAAwAA&iact=mrc&uact=8
  * https://www.google.fr/imgres?imgurl=http%3A%2F%2Fdocs.django-cms.org%2Fen%2Flatest%2F_images%2Fpage-list-expanded.png&imgrefurl=http%3A%2F%2Fdocs.django-cms.org%2Fen%2Flatest%2Fuser%2Freference%2Fpage_admin.html&docid=cx6m_ybuj9J93M&tbnid=1r6eR6F_j_1neM%3A&vet=10ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwh_KDcwNw..i&w=1036&h=578&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=0ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwh_KDcwNw&iact=mrc&uact=8
  * https://www.google.fr/imgres?imgurl=http%3A%2F%2Fwww.abidibo.net%2Fmedia%2Fblog%2Fmain_img%2Fscreenshot.png&imgrefurl=https%3A%2F%2Fwww.abidibo.net%2Fblog%2F2015%2F11%2F27%2Fshow-google-analytics-statistics-django-admin%2F&docid=Hr8FGu8ZbyqalM&tbnid=A5FMbgAdPwBx4M%3A&vet=10ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwhlKB0wHQ..i&w=800&h=357&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=0ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwhlKB0wHQ&iact=mrc&uact=8
  * https://www.google.fr/imgres?imgurl=https%3A%2F%2Fcms-assets.tutsplus.com%2Fuploads%2Fusers%2F1802%2Fposts%2F30035%2Fimage%2FiOS_11_Files_App.jpg&imgrefurl=http%3A%2F%2Fgracemanaay.tk%2F2017%2F11%2F30%2Fhow-to-update-your-app-for-ios-11-drag-and-drop%2F&docid=9sq8BCbe5WSIdM&tbnid=Ij1FCgjn0zW95M%3A&vet=12ahUKEwjIs8_It_rZAhVEtBQKHRqsDKw4ZBAzKEAwQHoECAAQQQ..i&w=850&h=638&itg=1&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=2ahUKEwjIs8_It_rZAhVEtBQKHRqsDKw4ZBAzKEAwQHoECAAQQQ&iact=mrc&uact=8
  * https://www.google.fr/imgres?imgurl=https%3A%2F%2Fi.imgur.com%2F8Qbrp0m.jpg&imgrefurl=https%3A%2F%2Fhostpresto.com%2Fcommunity%2Ftutorials%2Fhow-to-setup-a-python-django-website-on-hostpresto%2F&docid=uxN7D4J8uNj7uM&tbnid=ppL4baHkj_dx7M%3A&vet=12ahUKEwjIs8_It_rZAhVEtBQKHRqsDKw4ZBAzKCYwJnoECAAQJw..i&w=880&h=284&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=2ahUKEwjIs8_It_rZAhVEtBQKHRqsDKw4ZBAzKCYwJnoECAAQJw&iact=mrc&uact=8
  * https://www.google.fr/imgres?imgurl=https%3A%2F%2Fwww.abidibo.net%2Fmedia%2Fblog%2Fblog2017%2Findex-analytics-lg.jpg&imgrefurl=https%3A%2F%2Fwww.abidibo.net%2Fblog%2Fcategory%2Fdjango-admin%2F&docid=5gb9LJrE7sNuLM&tbnid=kNDk-ouzwHs-4M%3A&vet=10ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwimASheMF4..i&w=1400&h=855&client=ubuntu&bih=933&biw=1680&q=django%20snippet%20drag%20drop&ved=0ahUKEwjwwsqft_rZAhVEyRQKHc1vDOAQMwimASheMF4&iact=mrc&uact=8

Ideas
=====

  * A "Python script" plugin: allows to execute a python script on slave
  * Environment variable included: allows to defined information like "BALDENV_SLAVE_IP", "BALDENV_SLAVE_IP", ... Entered script is get as string, where baldenv are used like fstring (ex: {BALDENV_SLAVE_NAME}, then we made a `.format(**locals())` on it to get the right script
  * We could select the order of plugin execution, like a "pipeline" in Jenkins. A pipeline is an assembly of jobs. We have to get the two concept for a project
  * The repo analyze create a JSON which is used for each plugins
  * Each plugins must have po files for internationalisation
  * Baldryck have to manage webhook & sheduled task (day/hebdo/hours, month, year)
  * Plugins must to be delivered for baldryck as a zip renamed in ".bap" for baldryck plugin
  * plugins manager has to load plugin manually, not automatically
  * A get_available_plugin_list, then compare with BDD table, then alert if necessary.
  * A quote table, like bugzilla? with quote inspired by BlackAdder & Monthy Python, but python oriented modified

