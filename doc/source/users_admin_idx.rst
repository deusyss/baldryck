**************
Administration
**************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   users_admin_baldryck_management
   users_admin_plugins_management
   users_admin_slaves_management
   users_admin_users_management
   users_admin_groups_management
   users_admin_projects_management
   users_admin_jobs_management
