**************************
Installation documentation
**************************

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install_master_requirements
   install_slave_requirements
   install_master_cfg
   install_slave_cfg
