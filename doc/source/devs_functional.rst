**********
Functional
**********

Baldryck is only target for Linux hosting, with Gunicorn.

  #. Baldryck create a parralel process on host, dedicated to new job. Two queues are created
     to establish communication between Baldryck and job management process (JMP): one for order from
     Baldryck to JMP (Order Queue), and one for return from JMP to Baldryck (Feedback Queue).
  #. JMP create a SSH and SCP pipe between Baldryck host and slave
  #. JMP remote checkout deposit on slave
  #. JMP transfer baldryck_preparation folder and remote deploy the venv
  #. JMP remote get the project configuraton JSON file
  #. JMP remote launch baldryck analyse then the plugines analyze
  #. During scripts analyse, return is made to Baldryck using queues
  #. JMP transfer result to Baldryck host
  #. JMP delete the venv and the deposit clone folder
  #. Baldryck use the returns to update the DB and UI.


Plugins return
==============

Each plugin have to make a standard return to Baldryck as a Python dictionary.

Each dictionary must contain a key which name is the used tool name, in upper case. This will be useful
for plugin which embed several tool. As example, Pylint embeds Pylint and Pyreverse. So its returned
dictionary will contain two basic keys: "PYLINT" and "PYREVERSE".

Each tool dictionary can use one of the following key (and only one of them) to describe its content:


.. list-table::
  :widths: 25 75
  :header-rows: 1

  * - Key
    - description
  * - AVERAGE
    - Average star, to display as graphic with history of x build (parameter in Baldryck).
  * - TABLE
    - Data to display as a table. kay value will be a list of list. Idx0 will contain columns titles.
      If the same value appears several following times on same idx/line, so a cell fusion will be
      effective.
  * - STAR
    - Real build star, per file/folder/pkg. Tuple list: (relative file path from SRC path, star). As
      average, it will be display as a graphic with history.
  * - TXT
    - Tuples list: (relative analyze file path from SRC path, relative raw output file path). A link
      with star will be established.
  * - GRAPH
    - Tuples list: (relative analyze file path from SRC path, graphical path).
      Export format into SVG.
  * - OUTPUT
    - Link to a resume/csv/zip file


For a executed job, it will have a Baldryck dictionary that will contain a sub-dictionary per
plugin, as described previously. This Job dictionary will be use by Baldryck to supply the DB.
